
/**
 * Filtra um array contendo entradas do modelo de navegação e retorna apenas
 * as entradas pertencentes a um determinado grupo
 * 
 * @export
 * @class FilterOnNavGroupValueConverter
 */
export class FilterOnNavGroupValueConverter {
  toView(values, navGroup: String) {

    let groups = new Array();

    values.forEach( (item) => {
      if ( item.settings.navGroup === navGroup ) {
        groups.push(item);
      }
    });

    return groups;

  }

  fromView(value) {

  }
}

