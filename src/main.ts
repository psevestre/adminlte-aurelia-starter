import {Aurelia} from 'aurelia-framework'
import environment from './environment';
import authConfig from './authConfig';
import {I18N} from 'aurelia-i18n';
import Backend from 'i18next-xhr-backend';  
import {ValidationMessageProvider} from 'aurelia-validation';

//Configure Bluebird Promises.
//Note: You may want to use environment-specific configuration.
(<any>Promise).config({
  warnings: {
    wForgottenReturn: false
  }
});

export function configure(aurelia: Aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature('resources')
    .plugin('aurelia-animator-css')
    .plugin('aurelia-validation')
    .plugin('aurelia-auth', (baseConfig) => {
      baseConfig.configure(authConfig);
    })
    .plugin('aurelia-grid')
    .plugin('aurelia-i18n', (instance) => {

        // register backend plugin
        instance.i18next.use(Backend);

        // I18N / Validation setup
        ValidationMessageProvider.prototype.getMessage = function(key) {
          const translation = instance.tr(`errorMessages.${key}`);
          return this.parser.parseMessage(translation);
        };

        ValidationMessageProvider.prototype.getDisplayName = function(propertyName) {
          return instance.tr(propertyName);
        };        

        // adapt options to your needs (see http://i18next.com/docs/options/)
        // make sure to return the promise of the setup method, in order to guarantee proper loading
        return instance.setup({
          backend: {                                  // <-- configure backend settings
            loadPath: './locales/{{lng}}/{{ns}}.json', // <-- XHR settings for where to get the files from
          },
          lng : 'pt',
          attributes : ['t','i18n'],
          fallbackLng : 'pt',
          debug : false
        });
      })
    .plugin('aurelia-dialog');

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot());
}
