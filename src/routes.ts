// Módulo que armazena o array de rotas
import {RouteConfig} from 'aurelia-router';

var routes: RouteConfig[];

routes = [
       // Default
       { route: '', redirect: 'home' },

       // Login/logout
       { route: 'login', moduleId: './login', nav: false, title:'Login' },

			 //{ route: 'logout',        moduleId: './logout',       nav: false, title:'Logout' },       
      
       // Operação
       { 
          route: 'home',     
          name: 'console',  
          moduleId: 'home/index', 
          nav: true, 
          title: "menu.group1.home", 
          settings:{navGroup: 'operacao', navIndex:1},
          auth: true
       },
       { 
          route: 'planmonitor',  
          name: 'planmonitor',  
          moduleId: 'planmonitor/index', 
          nav: true, 
          title: "menu.operacao.planmonitor", 
          settings:{navGroup: 'operacao', navIndex:2},
          auth: true          
       },

       { 
         route: 'queuemonitor', 
         name: 'queuemonitor',   
         moduleId: 'queuemonitor/index', 
         nav: true, 
         title: "menu.operacao.queuemonitor",
         settings:{navGroup: 'operacao', navIndex:3},
         auth: true         
       },

       { 
         route: 'runjob',       
         name: 'runjob',   
         moduleId: 'runjob/index', 
         nav: true, 
         title: "menu.operacao.runjob", 
         settings:{navGroup: 'operacao', navIndex:4},
         auth: true
        },

       { 
         route: 'jobhistory',   
         name: 'jobhistory',   
         moduleId: 'jobhistory/index', 
         title: "menu.operacao.jobhistory", 
         nav: true, 
         settings:{navGroup: 'operacao', navIndex:5},
         auth: true
       },

       // Administração
       { 
          route: 'instances',   
          name: 'instances',   
          moduleId: 'instances/index', 
          nav: true, 
          title: "menu.admin.instances.title",
          settings:{navGroup: 'admin', navIndex:1},
          auth: true
       },

       { 
          route: 'instances/:id',   
          name: 'instances_edit',   
          moduleId: 'instances/edit', 
          nav: false, 
          title: "menu.admin.instances.edit.title",
          settings:{navGroup: 'admin', navIndex:1},
          auth: true
       },

       { 
          route: 'instances/:instanceId/pools/:poolId',   
          name: 'pool_edit',   
          moduleId: 'instances/pool', 
          nav: false, 
          title: "menu.admin.pools.edit.title",
          settings:{navGroup: 'admin', navIndex:1},
          auth: true
       },
       

       { 
         route: 'calendars',  
         name: 'calendars',   
         moduleId: 'calendars/index', 
         nav: true, 
         title: "menu.admin.calendars",
         settings:{navGroup: 'admin', navIndex:2},
         auth: true
       },
       { 
         route: 'plugins',   
         name: 'plugins',  
         moduleId: 'plugins/index', 
         nav: true, 
         title: "menu.admin.plugins",
         settings:{navGroup: 'admin', navIndex:3},
         auth: true
        },
       { 
         route: 'credentials',   
         name: 'credentials',   
         moduleId: 'credentials/index', 
         nav: true, 
         title: "menu.admin.credentials",
         settings:{navGroup: 'admin', navIndex:4},
         auth: true
       },
       { 
         route: 'logs',   
         name: 'logs',   
         moduleId: 'logs/index', 
         nav: true, 
         title: "menu.admin.logs",
         settings:{navGroup: 'admin', navIndex:5},
         auth: true
       }

       // Cadastros

       // Regras de Execução

       // Help
];

export default routes;
