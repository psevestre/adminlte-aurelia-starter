
import 'bootstrap';
 
import {inject} from 'aurelia-framework';
import {FetchConfig} from 'aurelia-auth';
import {Router, Redirect, NavigationInstruction, RouterConfiguration, RouteConfig } from 'aurelia-router';
import {AuthorizeStep,AuthService} from 'aurelia-auth';
import routes from './routes';
import {Profile} from './profile';

@inject(FetchConfig,routes,AuthService,Profile)
export class App {

  message: String;
  name: String;
  router: Router;
 
  constructor(
      private fetchConfig: FetchConfig, 
      private routes: Array<RouteConfig>, 
      private auth: AuthService,
      private profile : Profile
       ){
  }
 
  activate(){
    // Configuração do módulo de autenticação
    this.fetchConfig.configure();
  }

  /**
   * Configura as rotas de primeiro nível
   * 
   * @param {RouterConfiguration} config  
   */
  configureRouter(config: RouterConfiguration, router: Router) :void {

    this.router = router;
    config.title = "Userfront Monitor";
    config.addPipelineStep('authorize', AuthorizeStep);     
    config.map(this.routes);
  }

  get isAuthenticated() {
    return this.auth.isAuthenticated();
  }


  get currentUser() {
    return this.profile.currentUser;
  }

  logout() {
    this.auth.logout("/");
  }
}

