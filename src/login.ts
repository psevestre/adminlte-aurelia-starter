import {AuthService} from 'aurelia-auth';
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {Profile} from './profile';


@inject(AuthService,Router, Profile)
export class Login {


    username: String;
    password: String;
    loginError: String = "";

	constructor(private auth: AuthService,private router: Router,private profile: Profile){
	};

    login() {

        let target = this.auth['auth']['initialUrl']; 

        this.auth.login(this.username,this.password)
            .then(response => {
                return this.auth.getMe();
            })
            .then( userProfile => {

                this.profile.currentUser = userProfile;

                if ( target ) {
                    this.router.navigate(target);
                }
                else {
                    this.router.navigateToRoute("home");
                }                
            })
            .catch(err => {
                this.loginError = err;
            })

    }
};
