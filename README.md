# README #

This is a template project for admin applications based on the following stack:

* AdminLTE
* Aurelia

### Features

On top of all the regular features I´d expect from an AdminLTE-based template, we offer the following:

* Automatic side menu rendering based on the top-level routes
* Working login/logout flow that can be used locally
* I18N-ready thanks to aurelia-i18n integration
* Validation with aurelia-validation
* Mock API requests for quick development

### How do I get set up? ###

#### Install pre-requisites

* NodeJS version 4.x or above
* NPM version 3.x.x

#### Install dependencies

```
#!shell

npm install
```



